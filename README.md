# WPTM22

To run locally, clone the repo and run the following commands from the resulting directory:
```
npm i
npm start
```

## Deploying Updates

1. Test your changes locally.
2. Commit your work with a descriptive message outlining your changes on the main branch.
3. Push your changes (this will automatically trigger the GitLab Pages CI/CD process).
4. Monitor the build and deployment process by selecting "CI/CD > Pipeline" from the GitLab left-side menu.
5. Once you see two green checks*, test the changes out at https://ptmworkshop.gitlab.io/wptm22.

*If the build or deployment fails, the previous version of the site will remain published. Click on the failed stage (red X) to view the logs.
