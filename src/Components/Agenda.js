import React from "react";
import { useTable } from "react-table";

export default function Agenda(props) {

    const data = React.useMemo(
        () => [
          {
            time: '1:30-1:35',
            activity: 'Welcome',
          },
          {
            time: '1:35-1:45',
            activity: 'Introduction to Privacy Threat Modeling',
          },
          {
            time: '1:45-2:00',
            activity: 'MITRE Privacy Threat Taxonomy',
          },
          {
            time: '2:00-2:20',
            activity: 'LINDDUN Privacy Threat Model',
          },
          {
            time: '2:20-2:35',
            activity: 'Discussion & QA',
          },
          {
            time: '2:35-3:25',
            activity: 'Position paper presentations',
          },
          {
            time: '3:25-3:35',
            activity: 'Discussion & QA',
          },
          {
            time: '3:35-3:50',
            activity: 'BREAK',
          },
          {
            time: '3:50-4:30',
            activity: 'Break-out sessions',
          },
          {
            time: '4:30-5:00',
            activity: 'Full group discussion',
          },
          {
            time: '5:00',
            activity: 'Closing',
          },
        ],
        []
      )
    
      const columns = React.useMemo(
        () => [
          {
            Header: 'Time (ET)',
            accessor: 'time',
          },
          {
            Header: 'Activity',
            accessor: 'activity',
          },
        ],
        []
      )
    
      const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        rows,
        prepareRow,
      } = useTable({ columns, data })

    return (
        <div>
            <table {...getTableProps()} style={{border: 'none', borderBottm: 'solid 1px #DFDFDF', width: '100%' }}>
            <thead>
                {headerGroups.map(headerGroup => (
                <tr {...headerGroup.getHeaderGroupProps()}>
                    {headerGroup.headers.map(column => (
                    <th
                        {...column.getHeaderProps()}
                        style={{
                        borderBottom: 'solid 2px #DFDFDF',
                        background: 'white',
                        color: 'black',
                        fontWeight: 'bold',
                        }}
                        className='columns'
                    >
                        {column.render('Header')}
                    </th>
                    ))}
                </tr>
                ))}
            </thead>
            <tbody {...getTableBodyProps()}>
                {rows.map(row => {
                prepareRow(row)
                return (
                    <tr {...row.getRowProps()}>
                    {row.cells.map(cell => {
                        if (cell.row.id % 2 !== 0) {
                            return (
                                <td
                                    {...cell.getCellProps()}
                                    style={{
                                    padding: '5px 70px 5px 5px',
                                    border: 'solid 1px #DFDFDF',
                                    background: 'white',
                                    }}
                                >
                                    {cell.render('Cell')}
                                </td>
                            )
                        } else {
                            return (
                                <td
                                    {...cell.getCellProps()}
                                    style={{
                                    padding: '5px 70px 5px 5px',
                                    border: 'solid 1px #DFDFDF',
                                    background: '#F0FDF0',
                                    }}
                                >
                                    {cell.render('Cell')}
                                </td>
                            )
                        }
                        
                    })}
                    </tr>
                )
                })}
            </tbody>
            </table>
        </div>
    )
}