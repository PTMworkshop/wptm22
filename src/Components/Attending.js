import React from 'react';

export default function Attending(props) {
    let attend1 = 'The Workshop on Privacy Threat Modeling will be held in conjunction with the ';
    let attend2 = ', August 7-9 in Boston, MA. ';
    let attend2n = 'It is not necessary to attend SOUPS to attend the workshop, however, you must register for the workshop through SOUPS. The workshop registration fee is US $75.'
    let attend3 = 'The workshop will take place ';
    let attend4 = 'in person on Sunday, August 7th from 1:30-5PM';
    let attend5 = '. Please check back for specific location information – it will be updated as soon as we know.';
    let attend6 = 'Attendees may submit a 2-page position paper to be considered for a formal speaking role. Please see ';
    let attend7 = ' for details.';
    // let attend8 = 'Workshop registration is now open to all prospective attendees! All attendees must register here:';
    let attend_close = 'Workshop registration is now closed.'

    return (
        <div className='base-page'>
            <div className='attending-details'>
                <h4 className='heading'>Attendance</h4>
                <p>{attend1} 
                <a className='link' href='https://www.usenix.org/conference/soups2022' target='_blank' rel="noopener noreferrer">Symposium on Usable Privacy and Security (SOUPS) 2022</a>
                {attend2}
                <strong>{attend2n}</strong>
                </p>
                {/* <p></p> */}
                <p>{attend3}
                <strong>{attend4}</strong>
                {attend5}
                </p>
                <p>{attend6}
                <a className='link' href='/wptm22/#/submissions'>Submissions</a>
                {attend7}
                </p>

                {/* Registration link */}
                <ul style={{paddingLeft:'0px'}}>
                <li style={{display:'inline-block', paddingRight:'15px', fontWeight:'bold'}}>{attend_close}</li>
                {/* <li style={{display:'inline-block'}}><button disabled className='submit' onClick={()=> window.open("https://www.usenix.org/conference/277582/registration/form", "_blank", "noopener, noreferrer")}>
                    Register
                </button></li> */}
                </ul>
            </div>
            <div className='important-dates'>
                <h4 className='heading'>Important Dates</h4>
                {/* <p><strike style={{color:'#05A63A'}}><span style={{color:'black'}}>May 26, 2022 June 12, 2022 – Submission deadline</span></strike></p>
                <p><strike style={{color:'#05A63A'}}><span style={{color:'black'}}>June 9, 2022 June 17, 2022 – Invitation to speakers</span></strike></p> */}
                <p>July 18, 2022 – SOUPS early registration and Workshop submission deadline</p>
                <p>August 7-9, 2022 – The Symposium on Usable Privacy and Security</p>
            </div>
        </div>
    )
}