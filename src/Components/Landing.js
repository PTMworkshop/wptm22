import React from 'react';

export default function Landing(props) {

    let intro1 = 'The Workshop on Privacy Threat Modeling brings together researchers, practitioners, government representatives, and industry specialists to collaborate on the topic of privacy threats. While aspects of privacy risk modeling are relatively well-developed, such as constructions of privacy harms (Solove’s Taxonomy of Privacy and Calo’s subjective/objective privacy harms for instance), there has been insufficient discussion around approaches to modeling privacy threats, broadly construed. A holistic approach to representing privacy threats could inform privacy risk models and provide a common lexicon to accelerate conversations in the privacy community.';
    // let intro2 = 'We will explore how the community defines a privacy threat, incident, breach, or attack and the bounds of each term. We aim to develop better ways of creating datasets of privacy threats which can be used to generate threat models and better understand the privacy threat environment. We will discuss methods of categorizing and describing privacy threats using taxonomies and other ontological structures. The break-out sessions will be focused on innovative approaches to the research challenges in this space.';
    let intro2 = 'We will explore how the community defines a privacy threat, incident, breach, or attack and the bounds of each term. We aim to develop better ways of creating datasets of privacy threats which can be used to generate threat models and better understand the privacy threat environment. We will discuss methods of categorizing and describing privacy threats using taxonomies and other ontological structures, as well as research and implementation challenges in this space.'
    // let intro3 = 'The workshop will include an informative session and a collaborative session. In the informative session a panel of experts will offer perspectives on the nature and practice of privacy threat modeling, followed by two presentations on qualitative privacy threat models. The collaborative session will include break-out sessions to discuss how to define privacy threats and attacks, classify or categorize them, and apply research methods to the issues these present. ';
    let intro3 = 'The workshop will include an informative component and a collaborative component. In the informative component, two presentations describing qualitative privacy threat models will be followed by perspectives on the nature and practice of privacy threat modeling. The collaborative component will discuss issues related to the development and operationalization of privacy threat taxonomies.'
    let topics = ['Definitions of a privacy incident, attack, threat, and breach', 
                'Differences and similarities between privacy and cybersecurity threats',
                'Distinctions between privacy threats, privacy harms, and privacy vulnerabilities', 
                'Identifying and building datasets of privacy incidents, attacks, threats, and breaches',
                'Describing or categorizing privacy threats, including taxonomies or ontologies for privacy incidents, attacks, threats, and breaches',
                'Applicability and limitations of security threat modeling techniques for privacy',
                'Integration of threat models in risk models and risk management',
                'Role of risk modeling in privacy risk management',
                'Privacy threat-informed defense',
                'Qualitative versus quantitative threat modeling',
                'Privacy threat case studies'];
    let intro4 = 'See ';
    let intro5 = ' to view all submitted position papers.';
    

    return (
        <div className='base-page'>
            <h1 className='workshop-title'>The Workshop on Privacy Threat Modeling (WPTM)</h1>
            <h3 className='date-time-location'>Sunday, August 7th, 2022 | 1:30-5PM | In-person attendance</h3>
            <div className='intro-pars'>
                <p>{intro1}</p>
                <p>{intro2}</p>
                <p>{intro3}</p>
                <p>Topics of interest include:</p>
                <ul className='bulleted-list'>
                {topics.map((topic, index) => (
                    <li>{topic}</li>
                ))}
                </ul>
            </div>
            <p><b>
                {intro4}
                <a className='link' href='/wptm22/#/proceedings'>Proceedings</a>
                {intro5}
            </b></p>
            <div className='important-dates'>
                <h4 className='heading'>Important Dates</h4>
                {/* <p><strike style={{color:'#05A63A'}}><span style={{color:'black'}}>May 26, 2022 June 12, 2022 – Submission deadline</span></strike></p>
                <p><strike style={{color:'#05A63A'}}><span style={{color:'black'}}>June 9, 2022 June 17, 2022 – Invitation to speakers</span></strike></p> */}
                <p>July 18, 2022 – SOUPS early registration and Workshop submission deadline</p>
                <p>August 7-9, 2022 – The Symposium on Usable Privacy and Security</p>
            </div>
        </div>
    )
}