import React from 'react';
import slidesm from "../Static/MITRE-PTM-slides.pdf"
import slidesl from "../Static/LINDDUN-slides.pdf"
import slides_intro from "../Static/Shapiro-Intro-slides.pdf"
import paper3 from "../Static/wptm2022-paper3.pdf";
import slides3 from "../Static/Cronk-slides.pdf"
import paper5 from "../Static/wptm2022-paper5.pdf";
import slides5 from "../Static/Bookert-slides.pdf"
import paper6 from "../Static/wptm2022-paper6.pdf";
import slides6 from "../Static/Ghesmati-slides.pdf"
import paper7 from "../Static/wptm2022-paper7.pdf";
import paper8 from "../Static/wptm2022-paper8.pdf";
import paper9 from "../Static/wptm2022-paper9.pdf";
import slides9 from "../Static/Das-Naidu-slides.pdf"
import summary from "../Static/PTM-Workshop-Summary.pdf"

export default function Proceedings(props) {
    let abstract3 = 'Most discussions around privacy risks and threats begin and end with data processing, but privacy concerns predate the use of data, in the modern form, by millennia. Fundamentally, privacy exists at the boundary between the individual and others in society. It is where others interact with the individual or their proxies that privacy violations occur.'
    let abstract5 = 'The Internet of Medical Things (IoMT) is a frequent target of attacks -- compromising both patient data and healthcare infrastructure. While privacy-enhanced technologies and services (PETS) are developed to mitigate traditional privacy concerns, they cannot be applied without identifying specific threat models. Therefore, our position is that the new threat landscape created by the relatively new and underexplored IoMT domain must be studied. We briefly discuss specific privacy threats and threat actors in IoMT. Furthermore, we argue that the privacy policy gap needs to be identified for the IoMT threat landscape.';
    let abstract6 = 'While blockchain technologies leverage compelling characteristics in terms of decentralization, immutability and transparency, user privacy in public blockchains remains a fundamental challenge that requires a particular attention. This is mainly due to the history of all transactions being accessible and available to anyone, thus making it possible for an attacker to infer data about users that is supposed to remain private. In this paper, we provide a threat model of possible privacy attacks to users utilizing the Bitcoin blockchain. To this endeavor, we followed LINDDUN GO methodology to identify threats, and suggest possible mitigations.';
    let abstract7 = 'Causal questions often permeate in our day-to-day activities. With causal reasoning and counterfactual intuition, privacy threats can not only be alleviated but also prevented. In this paper, we discuss what is causal and counterfactual reasoning and how this can be applied in the field of privacy threat modelling (PTM). We believe that the future of PTM relies on how we can causally and counterfactually imagine cybersecurity threats and incidents.';
    let abstract8 = "Privacy risk assessments (PRAs) represent tools that aim to assist and guide organizations to ensure that their systems and services respect people’s privacy. By having organizations and systems' designers assess the impact on privacy that a proposed digital service or system may have before it is implemented, PRAs are meant to promote privacy-conscious products and designs [3]. Hence, PRAs have been touted as a useful instrument to promote privacy-by-design [18]. ";
    let abstract8_cont = "Scholars have praised the adoption of privacy risk assessments based on premises such as (1) they encourage a sense of responsibility on companies, embedding privacy considerations as part of their general risk management strategy; (2) they leverage organizations’ expertise where the regulator often does not have enough or adequate insight to deal with the complexities of the organization’s and its clients’ needs, as only the organization may have the sophisticated, insider knowledge about privacy expectations in the particular commercial sphere it operates; and (3) in our new datadriven economy, they offer a more flexible, adequate solution than traditional data protection principles and rights-based approaches to privacy [2, 10]. Critics on the other hand have pointed out that PRAs place unnecessary burdens on organizations, are difficult to carry out and may only lead to performative compliance [15, 17]."
    let abstract9 = 'Given the progressive nature of the world today, fairness is a very important social aspect in various areas, and it has long been studied with the advent of technology. To the best of our knowledge, methods of quantifying fairness errors and fairness in privacy threat models have been absent. To this end, in this short paper, we examine notions of fairness in privacy threat modelling due to different causes of privacy threats within a particular situation/context and that across contexts.';

    return (
        <div className='base-page'>
            <h4 className='heading'>Workshop Summary</h4>
            <div class='paper'>
                <h5>
                <a href={summary} target='_blank' rel="noopener noreferrer">
                Themes in Privacy Threat Modeling</a>
                </h5>
                <h6>Ben Ballard, Mark Paes, and Shelby Slotter, MITRE
                <br /> Ryan Xu, MITRE
                <br /> Julie McEwen, MITRE
                <br />Stuart Shapiro, MITRE
                <br />Cara Bloom, MITRE </h6>
            </div>
            <h4 className='heading'>Privacy Threat Models</h4>
            <div class='paper'>
                <h5>
                <a href={slidesm} target='_blank' rel="noopener noreferrer">
                MITRE Privacy Threat Taxonomy</a>
                </h5>
                <h6>Cara Bloom, MITRE 
                <br />Stuart Shapiro, MITRE</h6>
                <a class="link" href={slides_intro} target='_blank' rel="noopener noreferrer">
                Introduction to Privacy Threat Modeling</a>
            </div>
            <div class='paper'>
                <h5>
                <a href='https://www.linddun.org/' target='_blank' rel="noopener noreferrer">
                LINDDUN Privacy Threat Modeling</a>
                </h5>
                <h6>Laurens Sion, LINDDUN</h6>
                <a class="link" href={slidesl} target='_blank' rel="noopener noreferrer">
                Threat Modeling with LINDDUN</a>
            </div>
            <h4 className='heading'>Position Papers</h4>
            <div class='paper'>
                <h5>
                <a href={paper3} target='_blank' rel="noopener noreferrer">
                Interactions: a primitive for privacy threat modeling</a>
                </h5>
                <h6>R. Jason Cronk, Institute of Operational Privacy Design</h6>
                <p>{abstract3}</p>
                <a class="link" href={slides3} target='_blank' rel="noopener noreferrer">
                Slides</a>
            </div>
            <div class='paper'>
                <h5>
                <a href={paper5} target='_blank' rel="noopener noreferrer">
                Privacy Threats on the Internet of Medical Things</a>
                </h5>
                <h6>Nyteisha Bookert, North Carolina Agricultural and Technical State University
                <br />Mohd Anwar, North Carolina Agricultural and Technical State University</h6>
                <p>{abstract5}</p>
                <a class="link" href={slides5} target='_blank' rel="noopener noreferrer">
                Slides</a>
            </div>
            <div class='paper'>
                <h5>
                <a href={paper6} target='_blank' rel="noopener noreferrer">
                User Centric Public Blockchain Privacy Threats</a>
                </h5>
                <h6>Simin Ghesmati, Vienna University of Technology
                <br />Walid Fdhila, University of Vienna
                <br />Edgar Weippl, University of Vienna</h6>
                <p>{abstract6}</p>
                <a class="link" href={slides6} target='_blank' rel="noopener noreferrer">
                Slides</a>
            </div>
            <div class='paper'>
                <h5>
                <a href={paper7} target='_blank' rel="noopener noreferrer">
                Can Causal (and Counterfactual) Reasoning improve Privacy Threat Modelling?</a>
                </h5>
                <h6>Rakshit Naidu, Carnegie Mellon University
                <br />Navid Kagalwalla, Carnegie Mellon University</h6>
                <p>{abstract7}</p>
            </div>
            <div class='paper'>
                <h5>
                <a href={paper8} target='_blank' rel="noopener noreferrer">
                Privacy risk assessments: Sites of discretion, pseudoscience and performative compliance</a>
                </h5>
                <h6>Ero Balsa, Cornell Tech
                <br />Helen Nissenbaum, Cornell Tech</h6>
                <p>{abstract8} {abstract8_cont}</p>
            </div>
            <div class='paper'>
                <h5>
                <a href={paper9} target='_blank' rel="noopener noreferrer">
                Fair Context-Aware Privacy Threat Modelling</a>
                </h5>
                <h6>Saswat Das, National Institute of Science Education and Research, An OCC of Homi Bhabha National Institute, India
                <br />Rakshit Naidu, Carnegie Mellon University</h6>
                <p>{abstract9}</p>
                <a class="link" href={slides9} target='_blank' rel="noopener noreferrer">
                Slides</a>
            </div>
        </div>
    )
}