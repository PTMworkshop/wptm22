import React from 'react';
import Agenda from './Agenda';

export default function Program(props) {

    let linddun = ' is a privacy threat modeling methodology that supports analysts in systematically eliciting and mitigating privacy threats in software architectures. LINDDUN provides support to guide you through the threat modeling process in a structured way. In addition, LINDDUN provides privacy knowledge support to enable also non-privacy experts to reason about privacy threats. LINDDUN is a mnemonic for the privacy threat categories it supports: linkability, identifiability, non-repudiation, detectability, disclosure of information, unawareness, and non-compliance.';
    let taxonomy1 = 'MITRE is in the process of developing a taxonomy for describing and categorizing privacy threats. The Taxonomy was generated using a dataset of almost 150 privacy attacks identified from a set of FTC and FCC privacy cases that did not involve data theft or breaches. The taxonomy includes a linear attack path component, similar to the ';
    let taxonomy2 = ', and a contextual component that defines the socio-technical environment the attack took place in as well as the data types involved. The Taxonomy is presented in conjunction with Privacy Attack Patterns: generic instantiations of the types of attacks found in the FTC/FCC dataset. It is MITRE’s hope that the Privacy Attack Taxonomy can be used to model privacy threats, integrate into and inform privacy risk management programs, and be used to build a common knowledge base of privacy threats.';
    let pospapers1 = '*See ';
    let pospapers2 = ' to view all position papers and presentations.';

    return (
        <div className='base-page'>
            <h4 className='heading'>Program</h4>
            <div className='table'>
                <Agenda />
            </div>
            <div className='sessions'>
                <p>
                    {pospapers1}
                    <a className='link' href='/wptm22/#/proceedings'>Proceedings</a>
                    {pospapers2}
                </p>
                <h4 className='heading'>Break-out Sessions</h4>
                <h6>Designing Privacy Threat Models</h6>
                <ol>
                    <li>How should we scope privacy threat models (privacy vs. security, breadth vs. depth)?</li>
                    <li>Should we design privacy threat models from the top down or the bottom up?</li>
                    <li>How should we generate and use empirical data to create privacy threat models?</li>
                    <li>What is the appropriate level of detail for a privacy threat model?</li>
                    <li>How should privacy threat models account for intention (malicious vs. benign)?</li>
                    <li>How should privacy threat models distinguish between threats against a system and threats posed by a system?</li>
                </ol>

                <h6>Operationalizing Privacy Threat Models</h6>
                <ol>
                    <li>How can we integrate privacy threat models into risk modeling?​</li>
                    <li>Are privacy threat models required for risk management? ​</li>
                    <li>How can we get organizations to carry out privacy threat modeling/assessment?​</li>
                    <li>How can we make privacy threat modeling part of compliance regimes?​</li>
                    <li>How should privacy threat assessments be used?​</li>
                    <li>At what stage in the product lifecycle should privacy threat assessment be performed?​</li>
                    <li>How should privacy threat modeling be integrated with cybersecurity functions?​</li>
                    <li>What organizational role or function should own privacy threat modeling/assessment?​</li>
                </ol>
            </div>
            <div className='model'>
                <h4 className='heading'>About the LINDDUN Privacy Threat Model</h4>
                <p>
                <a className='link' href='https://www.linddun.org' target='_blank' rel="noopener noreferrer">LINDDUN</a>
                {linddun}
                </p>
            </div>
            <div className='model'>
            <h4 className='heading'>About the MITRE Privacy Threat Taxonomy</h4>
                <p> {taxonomy1}
                <a className='link' href='https://attack.mitre.org' target='_blank' rel="noopener noreferrer">MITRE ATT&CK Framework</a>
                {taxonomy2}
                </p>
            </div>
        </div>
    )
}