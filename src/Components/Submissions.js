import React from 'react';

export default function Submissions(props) {
    // let sub1 = 'The submission portal has reopened:'
    let sub2 = 'We are soliciting short position papers (2 pages excluding references and/or appendix using the SOUPS formatting template, found '
    let sub3 = ') related to privacy attacks, threats, and threat modeling. These may address:'
    let subList = ['Positions, arguments, or perspectives',
                    'Proposals of processes, methodologies, or solutions',
                    'Novel ideas',
                    'Previously published results',
                    'Works-in progress',
                    'Case studies',
                    'Preliminary results']
    let sub4 = 'Submissions should include author names and affiliations. Workshop papers will be made available to attendees prior to the workshop and will be posted on the workshop website. These submissions will not be considered “published” work and should not preclude publication elsewhere.';
    // let sub5 = 'Evaluation of papers for determining invitations will only occur if there are an overwhelming number of submissions, in which case 35 selected authors will be invited to the workshop.';
    let sub6 = 'Please email ';
    let sub7 = ' with questions.';
    // let reg1 ='Registration is now open and required for workshop attendance! Please see ';
    // let reg2 = ' for more information and to register.';
    let sub_close = 'The submission portal has closed.'
    
    return (
        <div className='base-page'>
            <div className='submission-details'>
                <h4 className='heading'>Submissions</h4>
                {/* Registration info - CLOSED */}
                {/* <p><strong>
                {reg1}
                <a className='link' href='/wptm22/#/attending'>Attending</a>
                {reg2}

                <hr style={{marginTop:'25px', marginBottom:'25px'}}></hr> */}

                {/* Submission info */}
                {/* </strong></p> */}
                {/* <ul style={{paddingLeft:'0px'}}>
                <li style={{display:'inline-block', paddingRight:'15px', fontWeight:'bold'}}>{sub1}</li>
                <li style={{display:'inline-block'}}><button className='submit' onClick={()=> window.open("https://wptm2022.usenix.hotcrp.com/", "_blank", "noopener, noreferrer")}>
                    Submit Position Paper
                </button></li>
                </ul> */}

                {/* ///////// SUBMISSIONS CLOSED AGAIN ///////// */}
                <p><strong>{sub_close}</strong></p>
                <p> {sub2}
                <a className='link' href='https://www.usenix.org/conference/soups2022/call-for-papers' target='_blank' rel="noopener noreferrer">here</a>
                {sub3}
                </p>
                <ul className='bulleted-list'>
                {subList.map((subType, index) => (
                    <li>{subType}</li>
                ))}
                </ul>
                <p>{sub4}</p>
                {/* <p>{sub5}</p> */}
                <p>{sub6}
                <a className='link' href='PTMworkshop@mitre.org'>PTMworkshop@mitre.org</a>
                {sub7}
                </p>
            </div>
            <div className='important-dates'>
                <h4 className='heading'>Important Dates</h4>
                {/* <p><strike style={{color:'#05A63A'}}><span style={{color:'black'}}>May 26, 2022 June 12, 2022 – Submission deadline</span></strike></p> */}
                {/* <p><strike style={{color:'#05A63A'}}><span style={{color:'black'}}>June 9, 2022 June 17, 2022 – Invitation to speakers</span></strike></p> */}
                <p>July 18, 2022 – SOUPS early registration and Workshop submission deadline</p>
                <p>August 7-9, 2022 – The Symposium on Usable Privacy and Security</p>
            </div>
        </div>
    )
}